function binarySearch(arr, searchNum){
    //find the middle point
    let midPoint = Math.floor(arr.length/2);
    //base case
    if(arr[midPoint] == searchNum){
        return "Found";
    }else{
        if(arr[midPoint] < searchNum){
            //set the array to the scope where the searching number is in using slice
            let newArr = arr.slice(midPoint+1, arr.length);
            //recursion
            return binarySearch(newArr, searchNum);
        }else{
            //set the array to the scope where the searching number is in using slice
            let newArr = arr.slice(0, midPoint);
             //recursion
            return binarySearch(newArr, searchNum);
        }
    }
}

var input = [1, 3, 4, 5, 8, 35, 78, 100, 210, 350];
console.log(binarySearch(input, 3));